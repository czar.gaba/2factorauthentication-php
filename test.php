
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
require_once 'vendor/autoload.php';

use Vectorface\GoogleAuthenticator;

$ga = new GoogleAuthenticator();
$secret = $ga->createSecret();
echo "Secret is: {$secret}\n\n";
echo "<hr>";
$qrCodeUrl = $ga->getQRCodeUrl('LORD_WINTER', $secret , "MYDEVHOUSE");
echo "PNG Data URI for the QR-Code: {$qrCodeUrl}\n\n";

?>
<img src="<?php  echo $qrCodeUrl;?>" alt="">
<?php 
echo "<hr>";
$oneCode = $ga->getCode($secret);
echo "Checking Code '$oneCode' and Secret '$secret':\n";
echo "<hr>";
// 2 = 2*30sec clock tolerance
$checkResult = $ga->verifyCode($secret, $oneCode, 2);
if ($checkResult) {
    echo 'OK';
} else {
    echo 'FAILED';
}?>


</body>
</html>
