<?php include('functions.php');?>
<?php include($partials.'header.php');?>
<?php 
// uncomment for session auto start
// session_starter();
require_once 'vendor/autoload.php';

use Vectorface\GoogleAuthenticator;

$ga = new GoogleAuthenticator();


if(isset($_SESSION['user'])){
    //header('location: ?security=check');
    $username = $_SESSION['user'];
    $data = custom_query("SELECT * FROM `tbl_user` where user='$username'");
    foreach ($data as $row) {
        $secret=$row['secret'];
    }
    // header('location: index.php?security=check');
}else{
    
}
?>
<body class="<?php fileclass();?>">

<?php 

if($_SERVER['REQUEST_METHOD']=="POST"){
    $conn = getConnection();


    //
if($_POST['process']=="register"){
    $conn = getConnection(); 
$u = $_POST['user'];
$p = md5($_POST['pass']);
$strr= "select * from tbl_user where user=:u";
$cm=$conn->prepare($strr);
$cm->bindParam(':u', $u);

$cm->execute();
$user = $cm->rowcount();

        if ($user == 0) {
            $secret = $ga->createSecret();
            $input = preg_replace("/[^a-zA-Z]+/", "", $_POST['name']);
            $qrCodeUrl = $ga->getQRCodeUrl($input, $secret , "MYDEVHOUSE");
        ?>
           <div class="section-1">
<div class="container">
        <div class="row">
            <div class="col-md-6">
                <form action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
                <input type="hidden" name="process" value="confirm">
                <input type="hidden" name="name" value="<?php echo $_POST['name'];?>">
                <input type="hidden" name="user" value="<?php echo $_POST['user'];?>">
                <input type="hidden" name="pass" value="<?php echo $_POST['pass'];?>">
                <input type="hidden" name="secret" value="<?php echo $secret;?>">
               

<div class="row">
    <div class="col-md-12">
        <h3> CONFIRM SECTION ||| QR Security Code</h3>
    </div>
    <div class="col-md-12">
    <img src="<?php  echo $qrCodeUrl;?>" alt="">
    </div>
    <div class="col-md-12">
        <h5>Download Google Authenticator</h5>
    </div>
    <div class="col-md-6"><a href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2&hl=en&gl=US"><img src="assets/images/google.png" class="img-fluid" alt=""></a></div>
    <div class="col-md-6"><a href="https://apps.apple.com/us/app/google-authenticator/id388497605"><img src="assets/images/apple.png" class="img-fluid" alt=""></a></div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            After installing Google Authenticator and scanning the QR place the Security Key here
            <br>    
            <input type="text" class="form-control" placeholder="Security Key *" name="security" required value=""/>
        </div>
    </div>
</div>
                    <input type="submit" value="submit" class="btn btn-primary">
                </form>
            </div>
        </div>
    </div>
</div>
        <?php 
            
        }else{
            //user already exist
            ?>
            <script>alert('Username Already Exist');</script>
            <script>
                window.location.href = './logout.php';
            </script>
            <?php
        }
}
    //
   if($_POST['process']=="login"){
    $u = $_POST['user'];
    $p = md5($_POST['pass']);
    $str= "select * from tbl_user where user=:u and pass=:p";
    $cm=$conn->prepare($str);
    $cm->bindParam(':u', $u);
    $cm->bindParam(':p', $p);
    $cm->execute();
    $user = $cm->rowcount();
    
    if ($user == 0) {
        ?>
          <script>alert('failed');</script>
          <script>
            window.location.href = './';
          </script>
        <?php
       
        
    }else{
        
        $_SESSION['user'] = $u;


        // get where field
        $data = get_where_fieldvalue('tbl_user','user',$u);
        foreach ($data as $row) {
            //echo $row['name']."<br />\n";
            $id= $row['id'];
        }

        ?>
          <script>alert('success');
          window.location.href = './index.php?security=check';
          </script>
          <?php

    }
   }

   //

   if($_POST['process']=="security"){

//
$oneCode=$_POST['security'];
    
    $checkResult = $ga->verifyCode($secret, $oneCode, 2);
    if ($checkResult) {
        ?>
        <script>alert('Security Passed');
        window.location.href = './success.php';
        </script>
        <?php
    } else {
        ?>
        <script>alert('Security Failed');
        window.location.href = './logout.php';
        </script>
        <?php
    }
//
   }


   //
if($_POST['process']=="confirm"){
    $oneCode=$_POST['security'];
    $secret=$_POST['secret'];
    $checkResult = $ga->verifyCode($secret, $oneCode, 2);
    if ($checkResult) {
        ?>
        <script>alert('Security Success');
        //  window.location.href = './logout.php';
        </script>
        <?php
        //filter user
        // insert function usage
        $array = array(
            'name'=>$_POST['name'],
            'user'=>$_POST['user'],
            'pass'=>md5($_POST['pass']),
            'secret'=>$secret
        );
        if(insert($array,'tbl_user')){
            session_destroy();
        ?>
        <script>alert('Register Success you can now login');</script>
       
        <script>
            window.location.href = './';
        </script>
        <?php
        }else{
            echo 'error on insert';
        }
       //
    } else {
        ?>
        <script>alert('Security Failed');
         window.location.href = './logout.php';
        </script>
        <?php
    }
}
   //
}else{
    // echo "not post req";
}

?>


<?php 

if(isset($_GET['security'])){
    ?>
<div class="section-1">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <form action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">

                <h3>Open your Google Authenticator and put the code here</h3>

                <input type="hidden" name="process" value="security">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Security *" name="security" required value=""/>
                    </div>
                    <input type="submit" value="submit" class="btn btn-primary">
                </form>
            </div>
        </div>
    </div>
</div>
    <?php
}elseif($_SERVER['REQUEST_METHOD']=="POST"){}else{
    ?>

<div class="section-1">
    <div class="container">
        <div class="row">
            <div class="col-md-6">

            <h2>Login Form</h2>
            <form action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
            <input type="hidden" name="process" value="login">
            <div class="form-content">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Username *" name="user" required value=""/>
                            </div>
                            
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="password" class="form-control" placeholder="Your Password *" name="pass" value=""/>
                            </div>
                           
                        </div>
                    </div>
                    <input type="submit" value="submit" class="btn btn-success">
                </div>
            </form>
            </div>
            <div class="col-md-6">
            <h2>Registration Form</h2>
                <!-- registration form  -->
                <form action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
                
                <input type="hidden" name="process" value="register">
                <div class="form-content">
                    
                   
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Your Name *" name="name" required value=""/>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Username *" name="user" required value=""/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="password" class="form-control" placeholder="Your Password *" id="pass1" name="pass" required value=""/>
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" placeholder="Confirm Password *" id="pass2" required value=""/>
                            </div>
                        </div>
                    </div>
                    
                    <input type="submit" value="submit" class="btn btn-info" id="btn-submit">
                </div>
                </form>
                <!-- end rform content  -->
            </div>
        </div>
    </div>
</div>
    <?php 
}

?>

 <?php include($partials.'footer.php');?>