# 2factorauthentication-php

Developer notes:
-------------
- working demo https://2fa.mydevhouse.dev/

Developer notes:
-------------
- Please ignore the vendor folder and delete
- database -> /database/2fa.sql
- create database in your localhost/server
- update database connection -> includes/conn.php

Installation:
-------------

- Use [Composer](https://getcomposer.org/doc/01-basic-usage.md) to
  install the package

```composer install```
- Credits to https://github.com/Vectorface/GoogleAuthenticator
<br><br>
#other reference: 
-------------
- https://gitlab.com/czar.gaba/self-class-library


#change Log for Authenticator Title
update vectorface/googleauthenticator/src/GoogleAuthenticator.php
```
public function getQRCodeUrl(string $name, string $secret) : string
    {
        $uri = "otpauth://totp/$name?secret=$secret";
        return 'data:image/png;base64,' . base64_encode($this->getQRCodeSRC($uri));
    }
```
to
```
public function getQRCodeUrl(string $name, string $secret , string $title) : string
    {
        $uri = "otpauth://totp/$name?secret=$secret&issuer=$title";
        return 'data:image/png;base64,' . base64_encode($this->getQRCodeSRC($uri));
    }
```


# Author

<h3>Czar G.</h3> 
<h3>Head Technical | Mydevhouse | Freelance Web Developer</h3> 
<h3><a href="https://mydevhouse.dev">https://mydevhouse.dev</a></h3> <br>
<a href="https://www.facebook.com/appleideath" target="_blank"><img src="https://img.icons8.com/doodle/50/000000/facebook-new.png"></a>
<a href="https://www.reddit.com/r/appleideath" target="_blank"><img src="https://img.icons8.com/doodle/48/000000/reddit--v4.png"></a>
<a href="https://twitter.com/appleideathxx" target="_blank"><img src="https://img.icons8.com/cotton/48/000000/twitter.png"></a>
<a href="https://www.instagram.com/appleideath/" target="_blank"><img src="https://img.icons8.com/officel/64/000000/instagram-new.png"></a>
<a href="https://gitlab.com/czar.gaba" target="_blank"><img src="https://img.icons8.com/color/48/000000/gitlab.png"></a>
<a href="https://www.linkedin.com/in/czar-gaba-b67a24124/" target="_blank"><img src="https://img.icons8.com/doodle/48/000000/linkedin--v2.png"></a>

<p> Love is the Death of Duty</p>